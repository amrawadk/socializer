---
home: true
heroText: Socializer
tagline: Socializing helper for busy professionals
actionText: Explore Details →
actionLink: /motivation/
features:
  - title: Whatsapp Integration
    details: Import contacts & send messages over whatsapp
  - title: Message Templates
    details: Send personalized messages to each contact
  - title: Message Rules
    details: Send messages to a selection of contacts on certain days
footer: Developed using VuePress ♥️
---
