---
prev: ./features
---

# Tools

I used the following tools to build the application

## Desktop App

- [Electron](https://www.electronjs.org/): a framework for building cross-platform desktop apps in JS, HTML & CSS
- [Vue](https://vuejs.org/): a JS framework, loved its simplicity.
- [Vuetify](https://vuetifyjs.com/en/): a Material Design Component Framework, has very useful components for building & styling the app.

## Documentation Website

- [VuePress](https://vuepress.vuejs.org/): very suitable for building static websites out of markdown, geared toward technical documentation.
- [Google Analytics](https://analytics.google.com/analytics/web/): to track metrics on the documentation website.
- [Gitlab CI](https://docs.gitlab.com/ee/ci/): Automate building and uploading the documentation to gitlab pages.
