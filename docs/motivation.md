---
prev: ./
next: ./features
---
# Motivation

Finding the right balance between family, work, social life is not an easy challenge, and I constantly find myself slowly getting out of touch with family & friends that I don't meet often. Also on special occasions like holidays, it's difficult to remember to contact everyone, and that results in some names being dropped.

There're some possible workarounds with broadcasting a message to a list of contacts, but that results in an impersonalized message that the reciepent can tell it was broadcasted, and hence doesn't bring have the same emotional impact. I wanted to be able to send a customized message for each person.

Additionally, I wanted to be able to automate this, making sure that it required minimal input from me to ensure all the messages are sent to the right people at the right times.