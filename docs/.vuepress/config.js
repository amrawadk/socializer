module.exports = {
  title: "Socializer",
  description: "Socializer | Socializing helper for busy professionals",
  themeConfig: {
    nav: [{ text: "Gitlab", link: "https://gitlab.com/AmrAwad/socializer" }],
    sidebar: ["/", "/motivation", "/features", "/tools"],
  },

  // Gitlab deployment config
  base: "/socializer/",
  dest: "public",

  plugins: [
    [
      "@vuepress/google-analytics",
      {
        ga: "UA-167947964-1",
      },
    ],
  ],
}
