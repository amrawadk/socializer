const { ipcRenderer } = require("electron")

var xpath = function(xpathToExecute) {
  var result = []
  var nodesSnapshot = document.evaluate(
    xpathToExecute,
    document,
    null,
    XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
    null
  )
  for (var i = 0; i < nodesSnapshot.snapshotLength; i++) {
    result.push(nodesSnapshot.snapshotItem(i))
  }
  return result
}

function getContactsInView(progress) {
  var matches = xpath(
    "//div[@id='pane-side']//div[contains(@style, 'z-index')]//span[@dir='auto']"
  )
  // xpath returns too many matches, to narrow down to contact names, we can check that title
  // matches the text
  matches = matches.filter(e => e.title === e.textContent)

  var contactNames = matches.map(c => c.title)
  // remove empty names
  contactNames = contactNames.filter(c => c !== "")
  console.log("contact Names", contactNames)
  ipcRenderer.sendToHost("contacts-response", progress, contactNames)
}

function scrollToView(sidePaneDiv, index) {
  sidePaneDiv.scrollTop = index * sidePaneDiv.clientHeight
  console.log("scrolling to index: ", index)
}

function getContacts() {
  const sidePaneDiv = xpath("//div[@id='pane-side']")[0]
  const viewCount = Math.ceil(
    sidePaneDiv.scrollHeight / sidePaneDiv.clientHeight
  )
  var index
  var progress
  // var contacts;
  for (index = 0; index < viewCount; index++) {
    // scroll to view
    setTimeout(
      function(idx) {
        scrollToView(sidePaneDiv, idx)
      },
      index * 2000,
      index
    )

    // read contacts from view into a list
    progress = ((index + 1) / viewCount) * 100
    setTimeout(
      function(progress) {
        getContactsInView(progress)
      },
      index * 2000 + 1500,
      progress
    )
    console.log("iteration -> ", index)
  }
}

function isSearchActive() {
  const searchBoxStatusElement = xpath('//*[@id="side"]/div[1]/div[1]')[0]
  return searchBoxStatusElement.classList.length > 1
}

function ActivateSearchBox() {
  const searchBoxDiv = xpath(
    "//div[@id='side']//div[@contenteditable='true']"
  )[0]

  if (!isSearchActive()) {
    console.log("search-inactive")
    searchBoxDiv.dispatchEvent(new MouseEvent("focus"))
    setTimeout(ActivateSearchBox, 1000)
  } else {
    console.log("search-active")
    ipcRenderer.sendToHost("search-box-in-focus")
  }
}
// communication from renderer to webview
ipcRenderer.on("contacts-request", event => {
  getContacts()
})

ipcRenderer.on("search-box-focus", event => {
  ActivateSearchBox()
  console.log("event-focused")
})

ipcRenderer.on("click-send-button", event => {
  const sendButton = xpath('//span[@data-icon="send"]')[0]
  sendButton.click()
  console.log("button-clicked")
})

ipcRenderer.on("choose-contact", (event, contactName) => {
  console.log(event, contactName)

  var matches = xpath(
    `//div[@id='pane-side']//div[contains(@style, 'z-index')]//span[@title='${contactName}']`
  )
  matches.sort(m => -m.getBoundingClientRect()["y"])
  const firstMatchElement = matches[0]
  console.log(firstMatchElement)
  firstMatchElement.click()
})

// Communication from webview to renderer
var connectionStatus = "connecting"

var observer = new MutationObserver(function(mutations, me) {
  var sidePane = document.getElementById("pane-side")
  if (sidePane && connectionStatus !== "connected") {
    // me.disconnect(); // stop observing
    connectionStatus = "connected"
    ipcRenderer.sendToHost("connection-status", connectionStatus)
  }
  var RetryConnectionButton = xpath("//div[contains(text(), 'Retry Now')]")
  if (RetryConnectionButton.length > 0 && connectionStatus !== "retrying") {
    connectionStatus = "retrying"
    ipcRenderer.sendToHost("connection-status", connectionStatus)
  }
})

// start observing
observer.observe(document, {
  childList: true,
  subtree: true,
})

// send an initial status
ipcRenderer.sendToHost("connection-status", connectionStatus)
