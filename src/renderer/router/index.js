import Vue from "vue"
import Router from "vue-router"

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: "/contacts",
      name: "contacts",
      component: require("@/components/Contacts").default,
    },
    {
      path: "/templates",
      name: "templates",
      component: require("@/components/Templates").default,
    },
    {
      path: "/messages/bulk",
      name: "bulk-messages",
      component: require("@/components/BulkMessages").default,
    },
    {
      path: "*",
      redirect: "/contacts",
    },
  ],
})
