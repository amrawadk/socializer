const state = {
  contacts: [],
}

const mutations = {
  SET_CONTACTS(state, { contacts }) {
    state.contacts = contacts
  },
  DELETE_CONTACT(state, { contact }) {
    const index = state.contacts.indexOf(contact)
    state.contacts.splice(index, 1)
  },
  EDIT_CONTACT(state, { contact }) {
    state.contacts = state.contacts.map(c => {
      return c.whatsappName === contact.whatsappName ? contact : c
    })
  },
  CREATE_CONTACT(state, { contact }) {
    state.contacts.push(contact)
  },
  APPEND_TO_CONTACTS(state, { contacts }) {
    state.contacts.push(...contacts)
  },
}

const actions = {
  updateContacts({ commit }, contacts) {
    commit("SET_CONTACTS", { contacts })
  },
  deleteContact({ commit }, contact) {
    commit("DELETE_CONTACT", { contact })
  },
  editContact({ commit }, contact) {
    commit("EDIT_CONTACT", { contact })
  },
  createContact({ commit }, contact) {
    commit("CREATE_CONTACT", { contact })
  },
  appendToContacts({ commit }, contacts) {
    commit("APPEND_TO_CONTACTS", { contacts })
  },
}

const getters = {
  contacts: state => {
    return state.contacts
  },
}

export default {
  state,
  getters,
  mutations,
  actions,
}
